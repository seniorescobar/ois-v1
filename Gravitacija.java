public class Gravitacija{
    public static void main(String args[]){
        double visina = Double.parseDouble(args[0]);
        izpis(visina, gravitacija(visina));
    }
    
    public static double gravitacija(double visina) {
        return 6.674 * Math.pow(10.0, -11.0) * 5.972 * Math.pow(10.0, 24.0) / Math.pow((5.972 * Math.pow(10.0, 24.0) * 6.371 * Math.pow(10.0, 6.0)), 2.0);
    }
    
    public static void izpis(double visina, double gravitacija) {
        System.out.println("Gravitacija na nadmorski visina: "+ visina+" je: "+gravitacija);
    }
}